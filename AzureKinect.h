#pragma once

/*
	Wrapper for Azure Kinect. 
	
	Written in a way to be more for general use whenver possible but with 
	a slight focus towards using Cinder.
	Technically will need GLM - Cinder uses glm by default.

	Currently no body tracking.

	Also credit to @prisonerjohn as this is pretty much porting his
	openFrameworks version.
	https://github.com/prisonerjohn/ofxAzureKinect

*/



#include <memory>
#include <string>
#include <k4a/k4a.hpp>

// comment out if you plan on handling things differently
#include "cinder/Cinder.h"

#include <iostream>


#ifdef CINDER_CINDER
#include "cinder/Surface.h"
#include "cinder/gl/Fbo.h"
#include "cinder/Log.h"
#include "cinder/Vector.h"
#include "cinder/ConcurrentCircularBuffer.h"
#include "cinder/gl/gl.h"
#include "cinder/app/App.h"
#else
#include <glm/vec2.hpp>
#endif

typedef std::shared_ptr<class AzureKinect>AZKinectRef;


typedef k4a_depth_mode_t DepthMode;
typedef k4a_color_resolution_t ColorResolution;
typedef k4a_image_format_t ImageFormat;
typedef k4a_fps_t FramesPerSecond;


class AzureKinect {
public:
	// define settings format
	// adjust all necessary settings including device selection with this object.
	class Format {
	public:
		Format():selectedDevice(K4A_DEVICE_DEFAULT) {
			numSensors = k4a_device_get_installed_count();
			
		}
		
		DepthMode depthMode() { return this->depthSettings; }
		ColorResolution colorResolution() { return colorSettings; }
		ImageFormat imageFormat() { return colorFormat; }
		FramesPerSecond framesPerSecond() { return this->fps; }
		bool shouldSyncImages() { return useSyncImages; }
		bool shouldUseColor() { return useColor; }
		bool shouldUseWorld() { return useWorld; }
		bool shouldUseIr() { return useIr; }
		int getTimeoutTime() { return timeoutTime; }

		uint32_t getDeviceIndex() { return selectedDevice;  }

		//! Set the device to use
		//! TODO add support for multiple devices. 
		void setDevice(uint32_t idx) {
			if (idx < numSensors) {
				selectedDevice = idx;
			}
			else {
				selectedDevice = 0;
				std::cout << "Selected device index is not valid" << std::endl;
			}
		}

		Format& setTimeoutTime(int mili) { timeoutTime = mili; }
		Format& irImage(bool ir) { useIr = ir; }
		Format& worlImage(bool world) { useWorld = world; }
		Format& colorImage(bool color) { useColor = color; }
		Format& syncImages(bool sync) { useSyncImages = sync; }
		Format& depthMode(DepthMode setting) { this->depthSettings = setting; return *this; }
		Format& colorResolution(ColorResolution setting) { this->colorSettings =  setting; return *this; }
		Format& imageFormat(ImageFormat setting) { colorFormat = setting; return *this; }
		Format& framesPerSecond(FramesPerSecond setting) { this->fps = setting; return *this; }
	private:

		int timeoutTime = 2000;

		uint32_t selectedDevice;
		uint32_t numSensors;
		DepthMode depthSettings = K4A_DEPTH_MODE_NFOV_2X2BINNED;
		//ColorResolution colorSettings = K4A_COLOR_RESOLUTION_2160P;
		ColorResolution colorSettings = K4A_COLOR_RESOLUTION_720P;

		ImageFormat colorFormat = K4A_IMAGE_FORMAT_COLOR_BGRA32;
		FramesPerSecond fps = K4A_FRAMES_PER_SECOND_30;

		// wait time until we timeout sensor lookup.
		uint16_t waitTime = 2000;
		bool useColor = true;
		bool useWorld = true;
		bool useIr = false;
		bool useSyncImages = false;
	};

public:
	AzureKinect(AzureKinect::Format format);
	


	static AZKinectRef create(AzureKinect::Format format) {
		return AZKinectRef(new AzureKinect(format));
	}

	//! Open a device 
	void open();

	//! Start device.
	void start();

	//! Stop a device
	void stop();

	void update();

	//! Returns whether or not the device is already open
	bool isOpen() const { return mOpen; }

	//! Returns whether or not the device is already streaming.
	bool isStreaming() const { return mStreaming; }

#ifdef CINDER_CINDER

	ci::Surface getColorImage() { 
		ci::Surface frame;

		if (mColorFrameQueue->tryPopBack(&frame)) {
			return frame;
		}
	}

	ci::Surface16uRef getDepthImage() { 
		if (hasFrame && depthSurface != nullptr && hasDepth) {
			return depthSurface;
		}
	}

#endif

	bool setupDepthToWorldTable();
	bool setupColorToWorldTable();
	bool setupImageToWorldTable(k4a_calibration_type_t type, k4a::image &img);

	bool hasNewFrame() { return hasFrame; }

	bool hasNewColor() { return hasColor; }
private:
	AzureKinect::Format format;
	
	bool hasFrame;

	uint32_t deviceIndex;
	bool mOpen;
	bool mStreaming;

	bool hasColor;
	bool hasIr;
	bool hasDepth;

#ifdef CINDER_CINDER

	//! Holds depth image information
	ci::Surface16uRef depthSurface;
	ci::SurfaceRef colorSurface;

	ci::Surface8u colorTest;

	ci::Surface16uRef depthToWorldPix;
	ci::Surface32fRef colorToWorldPix;

	ci::gl::TextureRef depthTexture, colorTexture;

	//! Frame queue to try and smoothly capture all images from the camera. 
	std::unique_ptr<ci::ConcurrentCircularBuffer<ci::Surface>> mColorFrameQueue;
#endif 

	// not totally sure what this is fore yet
	float * colorToWorldData;
	uint16_t * depthToWorldData;


	//! Serial number for the device
	std::string serialNumber;


	//! Stores current depth image
	k4a::image depthImage;

	//! Stores current color image
	k4a::image colorImage;

	//! Not totally sure yet why
	k4a::image colorToWorldImage;

	//! Also not totally sure why this extis. 
	k4a::image depthToWorldImage;


	k4a_device_configuration_t config;
	k4a::calibration calibration;
	k4a::transformation transformation;
	k4a::device device;
	k4a::capture capture;
	
};