
#include "AzureKinect.h"

using namespace std;
using namespace ci;
AzureKinect::AzureKinect(AzureKinect::Format format):
mOpen(false),
mStreaming(false),
hasColor(false),
hasDepth(false),
hasFrame(false){

	//setup config
	config = K4A_DEVICE_CONFIG_INIT_DISABLE_ALL;
	config.depth_mode = format.depthMode();
	config.color_resolution = format.colorResolution();
	config.camera_fps = format.framesPerSecond();
	config.color_format = format.imageFormat();
	config.synchronized_images_only = format.shouldSyncImages();

	deviceIndex = format.getDeviceIndex();

	// store format for later use.
	this->format = format;

	mColorFrameQueue = make_unique<ConcurrentCircularBuffer<ci::Surface>>(6);



}



void AzureKinect::open() {

	// if device is already open, ignore.
	if (mOpen) {
#ifdef CINDER_CINDER
		CI_LOG_I("Device is already open");
#else
		std::cout << "Device is already open;" << std::endl;
#endif
		return;
	}

	// attempt to open device
	try {
		device = k4a::device::open(deviceIndex);
		serialNumber = device.get_serialnum();
	

		mOpen = true;
	}
	catch (const k4a::error &e) {
#ifdef CINDER_CINDER

		CI_LOG_E("Error opening device -" << e.what());
#else
		std::cout << "Error opening device - " << e.what() << std::endl;

#endif
		device.close();
	}


#ifdef CINDER_CINDER
	CI_LOG_I("Opened device " << deviceIndex << " - serial number " <<serialNumber);
#else // CINDER_CINDER
	std::cout << "Opened device " << deviceIndex << std::endl;

#endif
}


void AzureKinect::start() {
	if (!mOpen) {
		std::cout << "no open devices" << std::endl;
		return;
	}

	// attempt to obtain calibration
	try {
		calibration = device.get_calibration(config.depth_mode, config.color_resolution);
	}
	catch (const k4a::error &e) {
#ifdef CINDER_CINDER

		CI_LOG_E("Error getting calibration-" << e.what());
#else
		std::cout << "Error getting calibration - " << e.what() << std::endl;

#endif
	}



	if (format.shouldUseColor()) {
		transformation = k4a::transformation(calibration);
	}

	// setup world LUT
	if (format.shouldUseWorld()) {
		setupDepthToWorldTable();

		if (format.shouldUseColor()) {
			//setup color to world LUT
			setupColorToWorldTable();
		}
	}

	// try starting camera
	try {
		device.start_cameras(&config);
		mStreaming = true;
	}
	catch (const k4a::error &e) {
#ifdef CINDER_CINDER

		CI_LOG_E("Error starting camera -" << e.what());
#else
		std::cout << "Error starting camera - " << e.what() << std::endl;

#endif


	}

}

void AzureKinect::stop() {
	if (mStreaming) {
		mStreaming = false;
	}

	transformation.destroy();
	device.stop_cameras();
}


void AzureKinect::update() {


	// reset flags 
	hasColor = false;
	hasDepth = false;

	// get frame
	try {
		if (!device.get_capture(&capture, std::chrono::milliseconds(2000))) {
#ifdef CINDER_CINDER

			CI_LOG_E("Timeout while trying to acquire frame");
#else
			std::cout << "Timeout while trying to process frame" << std::endl;

#endif
		}

		hasFrame = true;
	}
	catch (const k4a::error &e) {

		if (hasFrame) { hasFrame = false; }

#ifdef CINDER_CINDER

		CI_LOG_E("Error processing frame -" << e.what());
#else
		std::cout << "Error processing frame - " << e.what() << std::endl;

#endif
	}



	// get depth image
	depthImage = capture.get_depth_image();
	if (depthImage) {
	
		// when using Cinder, build surface for depth image.
#ifdef CINDER_CINDER
		// allocate a surface 
		uint16_t * rawDepth = reinterpret_cast<uint16_t*>(depthImage.get_buffer());
		depthSurface = ci::Surface16u::create(rawDepth, depthImage.get_width_pixels(), depthImage.get_height_pixels(), 1,ci::SurfaceChannelOrder::RGB);
		hasDepth = true;

#endif 
	}
	else {
#ifdef CINDER_CINDER
		CI_LOG_E("Unable to acquire depth image");
#else
		std::cout << "Unable to acquire depth image" << std::endl;
#endif 
	}


	// get color image if needed
	if (format.shouldUseColor()) {
		k4a::image color = capture.get_color_image();
		if (color) {
			
#ifdef CINDER_CINDER
			
			mColorFrameQueue->pushFront(ci::Surface(color.get_buffer(), color.get_width_pixels(), color.get_height_pixels(), color.get_width_pixels() * 4, ci::SurfaceChannelOrder::BGRA));
			hasColor = true;
#endif
		}
		else {
			std::cout << "Unable to get color image" << std::endl;
			return;
		}
	}

	// release current capture frame
	capture.reset();
	//hasFrame = false;
}

bool AzureKinect::setupDepthToWorldTable(){
	if (setupImageToWorldTable(K4A_CALIBRATION_TYPE_COLOR, depthToWorldImage)) {

		const int width = depthToWorldImage.get_width_pixels();
		const int height = depthToWorldImage.get_height_pixels();

		depthToWorldData = reinterpret_cast<uint16_t*>(depthToWorldImage.get_buffer());


#ifdef CINDER_CINDER
		
		depthToWorldPix = ci::Surface16u::create(depthToWorldData, width, height, width, ci::SurfaceChannelOrder::RGBA);
#endif 

		return true;
	}

	return false;
}

bool AzureKinect::setupColorToWorldTable() {
	if (setupImageToWorldTable(K4A_CALIBRATION_TYPE_COLOR, colorToWorldImage)) {

		const int width = colorToWorldImage.get_width_pixels();
		const int height = colorToWorldImage.get_height_pixels();

		colorToWorldData = reinterpret_cast<float*>(colorToWorldImage.get_buffer());

		
#ifdef CINDER_CINDER
		colorToWorldPix = ci::Surface32f::create(colorToWorldData, width, height, width * 4, ci::SurfaceChannelOrder::RGBA);
#endif 

		return true;
	}

	return false;
}

bool AzureKinect::setupImageToWorldTable(k4a_calibration_type_t type, k4a::image &img) {
	const k4a_calibration_camera_t& calibrationCamera = (type == K4A_CALIBRATION_TYPE_DEPTH) ? calibration.depth_camera_calibration : calibration.color_camera_calibration;
	const auto dims = glm::ivec2(
		calibrationCamera.resolution_width,
		calibrationCamera.resolution_height
	);

	try {
		img = k4a::image::create(K4A_IMAGE_FORMAT_CUSTOM, dims.x, dims.y, dims.x * static_cast<int>(sizeof(k4a_float2_t)));
	}
	catch (const k4a::error &e) {
#ifdef CINDER_CINDER

		CI_LOG_E("Error Calculating calibration image -" << e.what());
#else
		std::cout << "Error Calculating calibration image - " << e.what() << std::endl;

#endif
		return false;
	}

	auto imgData = reinterpret_cast<k4a_float2_t*>(img.get_buffer());
	k4a_float2_t p;
	k4a_float3_t ray;
	int idx = 0;

	for (int y = 0; y < dims.y; ++y) {
		p.xy.y = static_cast<float>(y);
		for (int x = 0; x < dims.x; ++x) {
			p.xy.x = static_cast<float>(x);
			if (calibration.convert_2d_to_3d(p, 1.f, type, type, &ray)) {
				imgData[idx].xy.x = ray.xyz.x;
				imgData[idx].xy.y = ray.xyz.y; 

			}
			else {
				// invalid pixel
				imgData[idx].xy.x = 0;
				imgData[idx].xy.y = 0;
			}

			++idx;
		}


	}
	return true;
}