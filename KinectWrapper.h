#pragma once

#include "Kinect.h"
#include "cinder/Log.h"
#include "cinder/gl/Texture.h"
#include "cinder/gl/gl.h"
#include "cinder/Surface.h"
#include "opencv2/core.hpp"
#include "CinderOpenCV.h"
using namespace ci;
using namespace std;


// Safe release for COM interfaces
template<class Interface>
inline void SafeRelease(Interface *& pInterfaceToRelease)
{
	if (pInterfaceToRelease != NULL)
	{
		pInterfaceToRelease->Release();
		pInterfaceToRelease = NULL;
	}
}
class KinectWrapper {

	// reference to the kinect sensor. 
	IKinectSensor * sensor = NULL;

	// Infrared reader - all that's needed in this case. 
	IInfraredFrameReader*  m_pInfraredFrameReader;
	//ILongExposureInfraredFrameReader * mLongExposureReader;

	// color reader. 
	IColorFrameReader * mColorFrameReader;

	BOOLEAN sensorAvailable = FALSE;
	BOOLEAN sourceAvailable = FALSE;
	BOOLEAN readerAvailable = FALSE;

	// ========== IR DATA ============= //
	Channel16u mIRCameraPixels;
	Surface8u mIRCameraPixelsSurface;
	ci::gl::TextureRef mIRCameraTexture;
	int irWidth, irHeight;

	// ========== COLOR DATA ============= //
	Surface mColorCameraPixels;
	ci::gl::TextureRef mColorCameraTexture;
	int colorWidth;
	int colorHeight;

	RGBQUAD * m_pColorRGBX;

	gl::Texture::Format fmt;
public:
	KinectWrapper() :irWidth(512), irHeight(424), colorWidth(1920), colorHeight(1080) {}
	void setup(std::function<void()> onReady = nullptr) {

		fmt.setInternalFormat(GL_RGB16);
		fmt.setWrap(GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE);
		fmt.minFilter(GL_NEAREST);
		fmt.magFilter(GL_NEAREST);

		// create heap storage for color pixel data in RGBX format
		m_pColorRGBX = new RGBQUAD[colorWidth * colorHeight];

		HRESULT hr;

		GetDefaultKinectSensor(&sensor);

		if (sensor) {
			hr = sensor->Open();

			if (SUCCEEDED(hr)) {

			}
			else {
				CI_LOG_I("Unable to open kinect");
			}

			initIR(hr);
			initColor(hr);
		}

		if (onReady != nullptr) {
			ci::Timer timer;

			while (sensorAvailable != TRUE) {

				timer.start();

				if (getAvailable()) {
					onReady();
					timer.stop();
				}

				if (timer.getSeconds() > 3) {
					throw(ci::Exception("Unable to get Kinect"));
					timer.stop();

				}

			}
		}
	}

	//! Returns whether or not the sensor is currently ready to use. 
	//! Startup does take time so best to use this in a loop of some sort. 
	BOOLEAN getAvailable() {
		sensor->get_IsAvailable(&sensorAvailable);
		return sensorAvailable;
	}

	void initColor(HRESULT hr) {
		mColorCameraTexture = gl::Texture::create(colorWidth, colorHeight, fmt);
		mColorCameraPixels = ci::Surface(colorWidth, colorHeight, true, SurfaceChannelOrder::RGBA);

		IColorFrameSource * colorSource = NULL;
		if (SUCCEEDED(hr))
		{
			hr = sensor->get_ColorFrameSource(&colorSource);
		}
		else {
			CI_LOG_I("failed to get color source");
		}

		if (SUCCEEDED(hr))
		{
			hr = colorSource->OpenReader(&mColorFrameReader);
		}
		else {
			CI_LOG_E("failed to get color reader");
		}

		SafeRelease(colorSource);
	}

	void initIR(HRESULT hr) {


		mIRCameraTexture = gl::Texture::create(irWidth, irHeight, fmt);
		mIRCameraPixels = ci::Channel16u(irWidth, irHeight);
		mIRCameraPixelsSurface = ci::Surface8u(irWidth, irHeight, false, SurfaceChannelOrder::BGR);

		// Initialize the Kinect and get the infrared reader
		IInfraredFrameSource* pInfraredFrameSource = NULL;


		if (SUCCEEDED(hr))
		{
			hr = sensor->get_InfraredFrameSource(&pInfraredFrameSource);
		}
		else {
			CI_LOG_I("failed to get IR source");
		}

		if (SUCCEEDED(hr))
		{
			hr = pInfraredFrameSource->OpenReader(&m_pInfraredFrameReader);
		}
		else {
			CI_LOG_E("failed to get IR reader");
		}

		SafeRelease(pInfraredFrameSource);
	}

	void update() {
		getIRFrameData();
		getColorFrameData();
	}

	void getColorFrameData() {
		IColorFrame* pColorFrame = NULL;

		HRESULT hr = mColorFrameReader->AcquireLatestFrame(&pColorFrame);

		if (SUCCEEDED(hr))
		{
			INT64 nTime = 0;
			IFrameDescription* pFrameDescription = NULL;
			int nWidth = 0;
			int nHeight = 0;
			ColorImageFormat imageFormat = ColorImageFormat_None;
			UINT nBufferSize = 0;
			RGBQUAD *pBuffer = NULL;

			hr = pColorFrame->get_RelativeTime(&nTime);

			if (SUCCEEDED(hr))
			{
				hr = pColorFrame->get_FrameDescription(&pFrameDescription);
			}

			if (SUCCEEDED(hr))
			{
				hr = pFrameDescription->get_Width(&nWidth);
			}

			if (SUCCEEDED(hr))
			{
				hr = pFrameDescription->get_Height(&nHeight);
			}

			if (SUCCEEDED(hr))
			{
				hr = pColorFrame->get_RawColorImageFormat(&imageFormat);
			}

			if (SUCCEEDED(hr))
			{
				if (imageFormat == ColorImageFormat_Bgra)
				{
					hr = pColorFrame->AccessRawUnderlyingBuffer(&nBufferSize, reinterpret_cast<BYTE**>(&pBuffer));
					CI_LOG_I("bgra");
				}
				else if (m_pColorRGBX)
				{
					pBuffer = m_pColorRGBX;
					nBufferSize = colorWidth * colorHeight * sizeof(RGBQUAD);
					hr = pColorFrame->CopyConvertedFrameDataToArray(nBufferSize, mColorCameraPixels.getData(), ColorImageFormat_Rgba);

				}
				else
				{
					hr = E_FAIL;
				}
			}

			if (SUCCEEDED(hr))
			{
				mColorCameraTexture->update(mColorCameraPixels);
			}

			SafeRelease(pFrameDescription);
		}

		SafeRelease(pColorFrame);
	}

	void getIRFrameData() {
		if (!m_pInfraredFrameReader) {
			return;
		}

		IInfraredFrame* pInfraredFrame = NULL;

		HRESULT hr = m_pInfraredFrameReader->AcquireLatestFrame(&pInfraredFrame);

		if (SUCCEEDED(hr)) {
			INT64 nTime = 0;
			IFrameDescription* pFrameDescription = NULL;
			int nWidth = 0;
			int nHeight = 0;
			UINT nBufferSize = 0;
			UINT16 *pBuffer = NULL;

			hr = pInfraredFrame->get_RelativeTime(&nTime);

			if (SUCCEEDED(hr))
			{
				hr = pInfraredFrame->get_FrameDescription(&pFrameDescription);
			}

			if (SUCCEEDED(hr))
			{
				hr = pFrameDescription->get_Width(&nWidth);
			}

			if (SUCCEEDED(hr))
			{
				hr = pFrameDescription->get_Height(&nHeight);
			}

			// update our stored data 
			if (irWidth != nWidth || irHeight != nHeight) {
				irWidth = nWidth;
				irHeight = nHeight;

				// re-initialize if height and width end up being different.
				mIRCameraPixels = Channel16u(irWidth, irHeight);
			}



			if (SUCCEEDED(hr))
			{
				hr = pInfraredFrame->AccessUnderlyingBuffer(&nBufferSize, &pBuffer);
			}

			if (SUCCEEDED(hr))
			{
				// copy pixel buffer to Channel. 
				pInfraredFrame->CopyFrameDataToArray(nBufferSize, mIRCameraPixels.getData());


				// update to Surface since we may need to do operations on the data later and 
				// a lot of OpenCV ops apparently don't work with grayscale.
				mIRCameraPixelsSurface = ci::Surface8u(mIRCameraPixels);

				// update camera texture so we can render 
				mIRCameraTexture->update(mIRCameraPixelsSurface);
				//mIRCameraTexture = gl::Texture::create(fromOcv(img));
			}

			SafeRelease(pFrameDescription);
		}

		SafeRelease(pInfraredFrame);
	}

	Channel16u getIRRawData() {
		return mIRCameraPixels;
	}

	Surface8u getIRData() {
		return mIRCameraPixelsSurface;
	}

	Surface8u getColorData() {
		return mColorCameraPixels;
	}

	void drawColor() {
		gl::ScopedMatrices mats;
		gl::translate(app::getWindowWidth(), 0);
		gl::scale(vec2(-1, 1));
		gl::draw(mColorCameraTexture);
	}

	void drawIR() {
		gl::ScopedMatrices mats;
		gl::translate(app::getWindowWidth(), 0);
		gl::scale(vec2(-1, 1));
		gl::draw(mIRCameraTexture);
	}
};